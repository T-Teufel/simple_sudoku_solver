# For use in CLI
# First argument: Path to file containing Soduko

import sys
from main import *

if len(sys.argv) != 2:
    raise IOError("Invalid number of Arguments")

build_string = file_to_string(sys.argv[1])
sudoku = Sudoku(build_string)

sudoku.display()
sudoku.solve()
sudoku.display()