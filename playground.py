from main import *

# Sudoku_0

build_string_0 = file_to_string("./Examples/Sudoku_0.txt")
sudoku_0 = Sudoku(build_string_0)

print("Sudoku_0:")

sudoku_0.display()
sudoku_0.solve()
sudoku_0.display()

# Sudoku_1

build_string_1 = file_to_string("./Examples/Sudoku_1.txt")
sudoku_1 = Sudoku(build_string_1)

print("Sudoku_1:")

sudoku_1.display()
sudoku_1.solve()
sudoku_1.display()