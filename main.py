# Constants

SUDOKU_GROUPS = [
    [(x, y) for x in range(0, 3) for y in range(0, 3)],
    [(x, y) for x in range(0, 3) for y in range(3, 6)],
    [(x, y) for x in range(0, 3) for y in range(6, 9)],
    [(x, y) for x in range(3, 6) for y in range(0, 3)],
    [(x, y) for x in range(3, 6) for y in range(3, 6)],
    [(x, y) for x in range(3, 6) for y in range(6, 9)],
    [(x, y) for x in range(6, 9) for y in range(0, 3)],
    [(x, y) for x in range(6, 9) for y in range(3, 6)],
    [(x, y) for x in range(6, 9) for y in range(6, 9)]
]

# Preconditions for use in Soduko class


def validate_build_string(build_string):
    if len(build_string) not in [89, 90]:
        raise SyntaxError("build_string is of invalid length. Too many line breaks?")

    for i in range(len(build_string)):
        if (i+1) % 10 == 0 and build_string[i] != '\n':
            raise SyntaxError("build_string cannot be split at position " + str(i))

        if (i+1) % 10 > 0 and (build_string[i] not in "0123456789"):
            raise SyntaxError("build_string contains invalid char at position " + str(i))


def validate_position(pos):
    if not (0 <= pos[0] <= 8 and 0 <= pos[1] <= 8):
        raise ValueError("Position " + str(pos) + " invalid.")


class Sudoku:
    def __init__(self, build_string=None):
        # 9*9 Sudoku-Grid
        self.grid = [[None for x in range(9)] for y in range(9)]

        # Metadata
        # amounts := Contains number of times a certain value (1-9) has not been inserted
        # empty_fields := Contains all empty fields
        self.amounts = [9] * 9
        self.empty_fields = [(x, y) for x in range(9) for y in range(9)]

        if build_string is not None:
            self.build(build_string)

    def display(self):
        print("")
        for row in self.grid:
            print(row)
        print("")

    # Getters and Setters for writing get_val((x, y)) instead of self.grid[y][x]

    def get_val(self, pos):
        validate_position(pos)
        return self.grid[pos[1]][pos[0]]

    def set_val(self, pos, val):
        validate_position(pos)
        self.grid[pos[1]][pos[0]] = val

    # Methods for initialization

    def update_metadata(self):
        self.empty_fields = []
        self.amounts = [9] * 9

        for x in range(9):
            for y in range(9):

                val = self.get_val((x, y))

                if val is None:
                    self.empty_fields.append((x, y))
                else:
                    self.amounts[val - 1] -= 1

    def build(self, build_string):
        validate_build_string(build_string)

        build_string = build_string.split('\n')

        for x in range(9):
            for y in range(9):
                val = int(build_string[y][x])
                if val > 0:
                    self.set_val((x, y), val)

        self.update_metadata()

    # Methods for solving the sudoku

    def validate_field(self, pos, val_conjecture=None):
        # Checks if the conjecture can be inserted at pos or
        # (if no value is given) if the value at pos is valid

        validate_position(pos)

        if val_conjecture is None:
            val = self.get_val(pos)
        else:
            val = val_conjecture

        if val is None:
            return True

        if val not in [1, 2, 3, 4, 5, 6, 7, 8, 9]:
            return False

        x, y = pos

        to_check = None

        for group in SUDOKU_GROUPS:
            if pos in group:
                to_check = group.copy()
                break

        for i in range(9):
            if (x, i) not in group:
                to_check.append((x, i))

            if (i, y) not in group:
                to_check.append((i, y))

        for field in to_check:
            if field != pos and self.get_val(field) == val:
                return False

        return True

    def solve(self):
        if len(self.empty_fields) == 0:
            return True

        pos = self.empty_fields.pop()

        for val in range(1, 10):
            if self.amounts[val-1] > 0 and self.validate_field(pos, val):

                self.amounts[val-1] -= 1
                self.set_val(pos, val)

                if not self.solve():

                    self.set_val(pos, None)
                    self.amounts[val - 1] += 1

                else:
                    return True

        self.empty_fields.append(pos)
        return False

# Misc. functions


def file_to_string(path):
    try:
        with open(path, 'r') as file:
            return file.read()

    except FileNotFoundError:
        raise FileNotFoundError("Path '" + path + "' does not exist.")
